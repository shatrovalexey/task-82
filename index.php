<?php
	require_once( 'vendor/autoload.php' ) ;

	use ashatrov\YMarket\Controller as YMController ;

	$config = require_once( 'config.php' ) ;
	$ctrlh = new YMController( $config ) ;

	header( 'Content-Type: application/json' ) ;

	echo json_encode( $ctrlh->execute( @$_REQUEST[ 'action' ] ) ) ;