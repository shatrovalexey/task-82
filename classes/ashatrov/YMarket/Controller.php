<?php
	namespace ashatrov\YMarket ;

	use ashatrov\YMarket\Model as YMModel ;

	class Controller {
		protected $ymh ;
		protected $config ;

		public function __construct( $config ) {
			$this->config = $config ;
		}

		protected function _ymh( ) {
			if ( ! empty( $this->ymh ) ) {
				return $this->ymh ;
			}

			$this->ymh = new YMModel( $this->config[ 'client_id' ] , $this->config[ 'client_secret' ] , $this->config[ 'token' ] ) ;

			return $this->ymh ;
		}

		protected function _method( $action ) {
			return 'action' . ucfirst( $action ) ;
		}

		public function execute( $action ) {
			$method = $this->_method( $action ) ;

			if ( ! method_exists( $this , $method ) ) {
				$method = $this->_method( 'help' ) ;
			}

			try {
				try {
					return $this->$method( ) ;
				} catch ( \Error $exception ) {
					throw new \Exception( $exception->getMessage( ) ) ;
				}
			} catch ( \Exception $exception ) {
				$error = $exception->getMessage( ) ;
				$email_sent = mail( $this->config[ 'email' ] , '������ ��� ���������� ������� �� yandex-market' , "
�����: $method
���������: $error
			" ) ;

				return [ 'error' => $error , 'mail_sent' => $email_sent , ] ;
			}
		}

		public function actionHelp( ) {
			$actions = [ ] ;

			foreach ( get_class_methods( static::class ) as $method_name ) {
				if ( ! preg_match( '{^action(.+)$}s' , $method_name , $matches ) ) {
					continue ;
				}

				$actions[] = lcfirst( $matches[ 1 ] ) ;
			}

			return [ 'actions' => $actions , ] ;
		}

		public function actionStats( ) {
			return [
				'offers' => $this->_ymh( )->getOffersCount( ) ,
				'hidden_offers' => $this->_ymh( )->getHiddenOffersCount( ) ,
			] ;
		}

		public function actionShow( ) {
			$offers = iterator_to_array( $this->_ymh( )->showAllOffers( ) ) ;
			$count = count( $offers ) ;

			return [
				'count' => $count ,
				'offers' => $offers ,
			] ;
		}

		public function actionHide( ) {
			$hidden_offers = iterator_to_array( $this->_ymh( )->hideAllOffers( ) ) ;
			$count = count( $hidden_offers ) ;

			return [
				'count' => $count ,
				'hidden_offers' => $hidden_offers ,
			] ;
		}

		public function actionHidden( ) {
			$hidden_offers = iterator_to_array( $this->_ymh( )->getHiddenOffersDataAll( ) ) ;

			return [
				'count' => count( $hidden_offers ) ,
				'hidden_offers' => $hidden_offers ,
			] ;
		}
	}