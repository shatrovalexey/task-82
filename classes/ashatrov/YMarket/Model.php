<?php
	namespace ashatrov\YMarket;

	use Yandex\OAuth\OAuthClient ;
	use Yandex\OAuth\Exception\AuthRequestException ;
	use Yandex\Market\Partner\Clients\BaseClient as YMBaseClient ;
	use Yandex\Market\Partner\Clients\HiddenOffersClient as YMHiddenOffersClient ;
	use Yandex\Market\Partner\Clients\AssortmentClient as YMAssortmentClient ;

	class Model {
		protected $token ;
		protected $client_id ;
		protected $client_secret ;

		const DELAY = 62 ;
		const STEP = 9 ;
		const SIZE = 100 ;

		protected static function delay( $i , $delay = self::DELAY ) {
			if ( ! $i || ( $i % self::STEP ) ) {
				return ;
			}

			sleep( $delay ) ;
		}

		public function __construct( $client_id , $client_secret , $token ) {
			$this->client_id = $client_id ;
			$this->client_secret = $client_secret ;
			$this->token = $token ;
		}

		public function getCampaigns( ) {
			$client = new YMBaseClient( $this->client_id , $this->token  ) ;

			for (
				$pageNumber = 1 ;
				(
					( $campaigns_object = $client->getCampaigns( [
						'page' => $pageNumber ,
					] ) ) &&
					( $pageNumber <= $campaigns_object->getPager( )->getPagesCount( ) )
				) ;
				$pageNumber ++
			) {
				for (
					$campaigns = $campaigns_object->getCampaigns( ) , $campaign = $campaigns->current( ) ;
					! empty( $campaign ) ;
					$campaign = $campaigns->next( )
				) {
					yield $campaign ;
				}
			}
		}

		public function showAllOffers( $params = [ ] ) {
			foreach ( $this->getCampaigns( ) as $campaign ) {
				$campaign_id = $campaign->getId( ) ;

				foreach ( $this->getAllOffersDataChained( $campaign_id , $params ) as $i => $offers ) {
					$result = $this->showOffers( $campaign_id , [ 'hiddenOffers' => $offers , ] ) ;

					if ( $result->getStatus( ) != 'OK' ) {
						continue ;
					}

					foreach ( $offers as $offer ) {
						yield $offer ;
					}

					static::delay( $i ) ;
				}
			}
		}

		public function hideAllOffers( $params = [ ] , $size = self::SIZE ) {
			$count = 0 ;

			foreach ( $this->getCampaigns( ) as $campaign ) {
				$campaign_id = $campaign->getId( ) ;

				foreach ( $this->getAllOffersDataChained( $campaign_id , $params , $size ) as $i => $offers ) {
					$result = $this->hideOffers( $campaign_id , [ 'hiddenOffers' => $offers , ] ) ;

					if ( empty( $result ) ) {
						continue ;
					}

					$count += count( $offers ) ;

					static::delay( $i ) ;
				}
			}

			return $count ;
		}

		protected function hideOffers( $campaign_id , $params = [ ] ) {
			return ( new YMHiddenOffersClient( $this->client_id , $this->token ) )
				->hideOffers( $campaign_id , $params ) ;
		}

		protected function showOffers( $campaign_id , $params = [ ] ) {
			return ( new YMHiddenOffersClient( $this->client_id , $this->token ) )
				->showOffers( $campaign_id , $params ) ;
		}

		protected function getAllOffersDataChained( $campaign_id , $params = [ ] , $size = 100 ) {
			foreach ( $this->getAllOffersChained( $campaign_id , $params , $size ) as $offers ) {
				$result = [ ] ;

				foreach ( $offers as $offer ) {
					$result[] = [
						'offerId' => $offer->getId( ) ,
						'feedId' => $offer->getFeedId( ) ,
					] ;
				}

				yield $result ;
			}
		}

		protected function getAllOffersChained( $campaign_id , $params = [ ] , $size = 100 ) {
			$offers = [ ] ;

			foreach ( $this->getAllOffers( $campaign_id , $params ) as $offer ) {
				$offers[] = $offer ;

				if ( count( $offers ) < $size ) {
					continue ;
				}

				yield $offers ;

				$offers = [ ] ;
			}

			if ( ! empty( $offers ) ) {
				yield $offers ;
			}
		}

		public function getAllOffers( $campaign_id , $params = [ ] ) {
			$client = new YMAssortmentClient( $this->client_id , $this->token ) ;

			for ( $params[ 'chunk' ] = 0 ; ; $params[ 'chunk' ] ++ ) {
				$offers_object = $client->getAllOffers( $campaign_id , $params ) ;
				$offer = $offers_object->current( ) ;

				if ( empty( $offer ) ) {
					break ;
				}

				while ( $offer ) {
					$count ++ ;
					yield $offer ;

					$offer = $offers_object->next( ) ;
				}
			}
		}

		public function getOffersCount( ) {
			$client = new YMAssortmentClient( $this->client_id , $this->token ) ;
			$count = 0 ;

			foreach ( $this->getCampaigns( ) as $campaign ) {
				$campaign_id = $campaign->getId( ) ;

				$count += $client->getOffers( $campaign_id )->getPager( )->getTotal( ) ;
			}

			return $count ;
		}

		public function getHiddenOffersCount( ) {
			$client = new YMHiddenOffersClient( $this->client_id , $this->token ) ;
			$count = 0 ;

			foreach ( $this->getCampaigns( ) as $campaign ) {
				$campaign_id = $campaign->getId( ) ;
				$hidden_offers_object = $client->getInfo( $campaign_id ) ;
				$count += $hidden_offers_object->getHiddenOffers( )->getTotal( ) ;
			}

			return $count ;
		}

		public function getHiddenOffersDataAll( $params = [ ] ) {
			foreach ( $this->getHiddenOffersAll( $params ) as $hidden_offer ) {
				yield (object)[
					'comment' => $hidden_offer->getComment( ) ,
					'feed_id' => $hidden_offer->getFeedId( ) ,
					'offer_id' => $hidden_offer->getOfferId( ) ,
					'ttl_in_hours' => $hidden_offer->getTtlInHours( ) ,
				] ;
			}
		}

		public function getHiddenOffersAll( $params = [ ] ) {
			foreach ( $this->getCampaigns( ) as $campaign ) {
				$campaign_id = $campaign->getId( ) ;

				foreach ( $this->getHiddenOffers( $campaign_id , $params ) as $offer ) {
					yield $offer ;
				}
			}
		}

		public function getHiddenOffers( $campaign_id , $params = [ ] ) {
			$client = new YMHiddenOffersClient( $this->client_id , $this->token  ) ;

			$params[ 'page_number' ] = 1 ;

			while ( true ) {
				$hidden_offers_object = $client->getInfo( $campaign_id , $params ) ;
				$offers = $hidden_offers_object->getHiddenOffers( ) ;

				foreach ( $offers as $hidden_offer ) {
					yield $hidden_offer ;
				}

				if ( $offers->getTotal( ) < $params[ 'page_number' ] * 5e2 ) {
					break ;
				}

				$params[ 'page_number' ] ++ ;
			}
		}
	}